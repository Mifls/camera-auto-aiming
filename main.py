import cv2
import time

from ultralytics import YOLO

from cv2_utils import draw_predicted_boxes, make_required_rectangle, make_circumscribed_rectangle, find_center, \
    make_target_rectangle

CONFIDENCE = 0.5
font_scale = 1
thickness = 1
labels = open("data/coco.names").read().strip().split("\n")
print(labels)
color = [255, 0, 0]
required_color = [0, 0, 255]
circumscribed_color = [0, 255, 255]
target_color = [0, 255, 0]
model = YOLO("best.pt")

cap = cv2.VideoCapture(0)
_, image = cap.read()
h, w = image.shape[:2]
ratio = w / h
while True:
    _, image = cap.read()

    start = time.perf_counter()
    # run inference on the image
    results = model.predict(image, conf=CONFIDENCE)[0]

    boxes = results.boxes.data.tolist()
    image = draw_predicted_boxes(image=image,
                                 boxes=boxes,
                                 labels=labels,
                                 color=color,
                                 thickness=thickness,
                                 font_scale=font_scale)

    rectangle = make_required_rectangle(boxes=boxes,
                                        image=image,
                                        color=required_color,
                                        thickness=thickness)

    if rectangle:
        rectangle = make_circumscribed_rectangle(rectangle=rectangle,
                                                 border=0.1,
                                                 ratio=ratio,
                                                 image=image,
                                                 color=circumscribed_color,
                                                 thickness=thickness)

        center = find_center(boxes, image, target_color)
        rectangle = make_target_rectangle(rectangle=rectangle,
                                          center=center,
                                          ratio=ratio,
                                          image=image,
                                          color=target_color,
                                          thickness=thickness)

    # end time to compute the fps
    end = time.perf_counter()
    # calculate the frame per second and draw it on the frame
    fps = f"FPS: {1 / (end - start):.2f}"
    cv2.putText(image, fps, (50, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 6)

    cv2.imshow("image", image)

    if ord("q") == cv2.waitKey(1):
        break

cap.release()
cv2.destroyAllWindows()
