import cv2


def draw_predicted_boxes(image, boxes, labels, color, thickness, font_scale):
    for data in boxes:
        # get the bounding box coordinates, confidence, and class id
        xmin, ymin, xmax, ymax, confidence, class_id = data
        # converting the coordinates and the class id to integers
        xmin = int(xmin)
        ymin = int(ymin)
        xmax = int(xmax)
        ymax = int(ymax)
        class_id = int(class_id)

        # draw a bounding box rectangle and label on the image
        cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color=color, thickness=thickness)
        text = f"{labels[class_id]}: {confidence:.2f}"
        # calculate text width & height to draw the transparent boxes as background of the text
        (text_width, text_height) = \
            cv2.getTextSize(text, cv2.FONT_HERSHEY_SIMPLEX, fontScale=font_scale, thickness=thickness)[0]
        text_offset_x = xmin
        text_offset_y = ymin - 5
        box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width + 2, text_offset_y - text_height))
        overlay = image.copy()
        cv2.rectangle(overlay, box_coords[0], box_coords[1], color=color, thickness=cv2.FILLED)
        # add opacity (transparency to the box)
        image = cv2.addWeighted(overlay, 0.6, image, 0.4, 0)
        # now put the text (label: confidence %)
        cv2.putText(image, text, (xmin, ymin - 5), cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=font_scale, color=(255, 150, 150), thickness=thickness)
    return image


def make_required_rectangle(boxes, image, color, thickness):
    if len(boxes) == 0:
        return None
    boxes = list(zip(*boxes))
    xmin = min(boxes[0])
    ymin = min(boxes[1])
    xmax = max(boxes[2])
    ymax = max(boxes[3])

    cv2.rectangle(image,
                  (int(xmin), int(ymin)), (int(xmax), int(ymax)),
                  color=color, thickness=thickness)

    return xmin, ymin, xmax, ymax


def make_circumscribed_rectangle(rectangle, border, ratio, image, color, thickness):
    xmin, ymin, xmax, ymax = rectangle
    width = xmax - xmin
    height = ymax - ymin

    dx = width * border
    xmin -= dx
    xmax += dx

    dy = height * border
    ymin -= dy
    ymax += dy

    width = xmax - xmin
    height = ymax - ymin

    if height * ratio < width:
        delta = (width / ratio - height) * 0.5
        ymin -= delta
        ymax += delta
    else:
        delta = (height * ratio - width) * 0.5
        xmin -= delta
        xmax += delta

    cv2.rectangle(image,
                  (int(xmin), int(ymin)), (int(xmax), int(ymax)),
                  color=color, thickness=thickness)

    return xmin, ymin, xmax, ymax


def find_center(boxes, image, color):
    if len(boxes) == 0:
        return None

    x = 0
    y = 0
    for box in boxes:
        xmin, ymin, xmax, ymax, _, _ = box
        x += (xmin + xmax) * 0.5
        y += (ymin + ymax) * 0.5

    x /= len(boxes)
    y /= len(boxes)

    cv2.circle(image,
               (int(x), int(y)), 2,
               color, thickness=cv2.FILLED)

    return x, y


def make_target_rectangle(rectangle, center, ratio, image, color, thickness):
    xmin, ymin, xmax, ymax = rectangle
    x, y = center

    dx = max(x - xmin, xmax - x)
    dy = max(y - ymin, ymax - y)
    xmin = x - dx
    ymin = y - dy
    xmax = x + dx
    ymax = y + dy

    return make_circumscribed_rectangle(rectangle=(xmin, ymin, xmax, ymax),
                                        border=0,
                                        ratio=ratio,
                                        image=image,
                                        color=color,
                                        thickness=thickness)
